<?php

return [
    'ROLE_ADMIN' => 2,
    'ROLE_USER' => 1,
    'ORDER_STATUS'=>[
        'PENDING'=>'pending',
        'PREPARING'=>'preparing',
        'CANCELED'=>'cancelled',
        'CARGO'=>'in-cargo',
        'DELIVERED'=>'delivered'
    ]
];
