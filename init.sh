#!/bin/bash

php artisan migrate

php artisan passport:install

php artisan key:generate

php artisan db:seed

php artisan storage:link
