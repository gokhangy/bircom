<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::truncate();

        DB::table('users')->insert([
            'name' => 'Gökhan YNR',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'email_verified_at'=>\Carbon\Carbon::now(),
            'role' => config('constant.ROLE_ADMIN'),
            'created_at'=>\Carbon\Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Bircom',
            'email' => 'user@bircom.com',
            'password' => Hash::make('123456'),
            'email_verified_at'=>\Carbon\Carbon::now(),
            'role' => config('constant.ROLE_USER'),
            'created_at'=>\Carbon\Carbon::now(),
        ]);
    }
}
