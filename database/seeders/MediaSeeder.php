<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Plank\Mediable\Media;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Media::truncate();

        DB::table('media')->insert([
            'disk' => 'public',
            'directory' => '1',
            'filename' => 'https://via.placeholder.com/150',
            'extension' => 'png',
            'mime_type' => 'image/*',
            'size' => 102,
            'aggregate_type'=>'test'
        ]);

        DB::table('media')->insert([
            'disk' => 'public',
            'directory' => '1',
            'filename' => 'https://via.placeholder.com/100',
            'extension' => 'png',
            'mime_type' => 'image/*',
            'size' => 102,
            'aggregate_type'=>'test'
        ]);
    }
}
