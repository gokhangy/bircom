<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UserSeeder::class]);
        $this->call([MediaSeeder::class]);
        $this->call([ProductSeeder::class]);
        $this->call([PaymentTypeSeeder::class]);
        $this->call([StatusSeeder::class]);
    }
}
