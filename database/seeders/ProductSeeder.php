<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::truncate();

        DB::table('products')->insert([
            'title' => 'Book 1',
            'slug'=>'book-1',
            'content' => 'bir-com-description-test',
            'image_id' => '1',
            'price' => '100',
            'user_id' => '1',
            'updated_at'=>\Carbon\Carbon::now(),
            'created_at'=>\Carbon\Carbon::now(),
        ]);

        DB::table('products')->insert([
            'title' => 'Book 2',
            'slug'=>'book-2',
            'content' => 'bir-com-description-test-2',
            'image_id' => '2',
            'price' => '100.6',
            'user_id' => '1',
            'updated_at'=>\Carbon\Carbon::now(),
            'created_at'=>\Carbon\Carbon::now(),
        ]);
    }
}
