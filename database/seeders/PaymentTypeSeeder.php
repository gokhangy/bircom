<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::truncate();

        DB::table('payment_types')->insert([
            'name' => 'Credit Cart',
            'slug'=>'credit-card',
            'status'=>'1',
            'updated_at'=>\Carbon\Carbon::now(),
            'created_at'=>\Carbon\Carbon::now(),
        ]);

        DB::table('payment_types')->insert([
            'name' => 'Cash',
            'slug'=>'cash',
            'status'=>'1',
            'updated_at'=>\Carbon\Carbon::now(),
            'created_at'=>\Carbon\Carbon::now(),
        ]);

        DB::table('payment_types')->insert([
            'name' => 'Pay at the door',
            'slug'=>'Pay-at-the-door',
            'status'=>'1',
            'updated_at'=>\Carbon\Carbon::now(),
            'created_at'=>\Carbon\Carbon::now(),
        ]);
    }
}
