<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'status' => 'Pending',
            'slug' => config('constant.ORDER_STATUS.PENDING'),
            'isActive'=>'1',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('status')->insert([
            'status' => 'Preparing',
            'slug' => config('constant.ORDER_STATUS.PREPARING'),
            'isActive'=>'1',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('status')->insert([
            'status' => 'In Cargo',
            'slug' => config('constant.ORDER_STATUS.CARGO'),
            'isActive'=>'1',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('status')->insert([
            'status' => 'Delivered',
            'slug' => config('constant.ORDER_STATUS.DELIVERED'),
            'isActive'=>'1',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }
}
