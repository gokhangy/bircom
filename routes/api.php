<?php

use App\Http\Controllers\Api\Admin\ProductController;
use App\Http\Controllers\Api\User\OrderController;
use App\Http\Controllers\Api\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Api\User\ProductController as UserProductController;
use App\Http\Controllers\Api\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->middleware('api')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('register', [AuthController::class, 'register']);
});

Route::middleware(['auth:api', 'role_admin'])->prefix('admin')->group(function () {

    Route::post('products/{product}', [ProductController::class, 'update']);
    Route::apiResources(['products' => ProductController::class]);
    Route::apiResources(['orders' => AdminOrderController::class]);
    Route::get('status', [AdminOrderController::class, 'getStatus']);

});

Route::middleware(['auth:api', 'role_user'])->prefix('user')->group(function () {

    Route::apiResources(['products' => UserProductController::class]);
    Route::get('/payments', [OrderController::class, 'payments']);
    Route::post('/orders', [OrderController::class, 'store']);

});
