

## Installation with Docker

###Backend (Laravel 8)
- Inside project root directory at terminal
- run docker-compose up -d

For database migration,seed,key, and passport
- run docker exec -it app bash
- composer install

- run ./init.sh

Or  you can run manuel if you want

- php artisan migrate
 
 - php artisan passport:install
 
 - php artisan key:generate
 
 - php artisan db:seed
 
 - php artisan storage:link


### Frontend (Angular 9)

- nodejs is required.

inside project root directory 

- cd resources/frontend

- run npm install
- run npm run build

#### Default record for login 
ROLE_ADMIN:
email: admin@admin.com
password: 123456

ROLE_USER:
 email: user@bircom.com
 password: 123456
 
###Database Access (Postgresql)
- url: bircom.local:8044

- username: "info@bircom.com"
- password: "bircom2020!"

- you can see db access data in .env file in project directory

 ###Not : 
 - You have to run domain name : bircom.local . 
 - Because of set backend service api url inside frontend

or you can change it here if you want

- cd resources/frontend/src/environments/
- file :  environments.ts and,  environments.prod.ts
