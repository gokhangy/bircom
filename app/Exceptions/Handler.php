<?php

namespace App\Exceptions;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function register()
    {

    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @param $request
     * @param Throwable $exception
     * @return JsonResponse
     */
    public function render($request, Throwable $exception)
    {
        $response = new BaseController();

        if ($exception instanceof ModelNotFoundException) {

            $message = '[ ' . str_replace('\\App', '', $exception->getModel()) . ' not found ]'
                . ' [ Error Line : ' . $exception->getLine() . ' ] [ Message : ' . $exception->getMessage();

            Log::error($message);

            return $response->apiResponse(
                $response::ERROR, null,
                trans('messages.not_found_record'),
                $response::HTTP_NOT_FOUND);

        } elseif ($exception instanceof NotFoundHttpException) { // 404


            $message = ' [ Error Line : ' . $exception->getLine() . ' ] [ Message : ' . $exception->getMessage();

            Log::error($message);

            return $response->apiResponse(
                $response::ERROR, null,
                trans('messages.page_not_found'),
                $response::HTTP_NOT_FOUND);

        } elseif ($exception instanceof AuthenticationException) {

            $message = '[ unauthorized access ] [ Error Line : ' . $exception->getLine() . ' ] [ Message : ' . $exception->getMessage();

            Log::error($message);

            return $response->apiResponse(
                $response::ERROR, null,
                trans('messages.unauthorized'),
                $response::HTTP_UNAUTHORIZED);

        } elseif (($exception instanceof QueryException) && env('APP_ENV') !== 'local') {

            $message = '[ Error Line : ' . $exception->getLine() . ' ] [ Message : ' . $exception->getSql();

            Log::error($message);

            return $response->apiResponse(
                $response::ERROR, null,
                trans('messages.wrong_query'),
                $response::HTTP_INTERNAL_SERVER_ERROR);

        } elseif ($exception instanceof ValidationException) {

            $message = '[ validation failed ] [ Error Line : ' . $exception->getLine() . ' ] [ Message : ' . $exception->getMessage();

            Log::error($message);

            return $response->apiResponse(
                $response::ERROR, null,
                trans('messages.validation_error'),
                $response::HTTP_UNPROCESSABLE_ENTITY);

        } else {

            return $response->apiResponse(
                $response::ERROR, null,
                trans('messages.internal_server_error'),
                $response::HTTP_INTERNAL_SERVER_ERROR);

            // return parent::render($request, $exception);
        }

    }
}
