<?php


namespace App\Traits;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Plank\Mediable\Exceptions\MediaUploadException;
use Plank\Mediable\HandlesMediaUploadExceptions;
use Plank\Mediable\Media;
use MediaUploader;


trait MediaTrait
{
    use FileManagerTrait;
    use HandlesMediaUploadExceptions;

    /**
     * @param $file
     * @return mixed
     * @throws \Exception
     */
    public function uploadFile($file)
    {

        try {

            $media = MediaUploader::fromSource($file)
                ->useHashForFilename()
                ->toDirectory('temporary')
                ->upload();
            $media->move($media->id);
            return $media;

        } catch (MediaUploadException $e) {
            $this->logError($e, trans('messages.file_error'), __LINE__, __METHOD__);
            throw $this->transformMediaUploadException($e);
        }
    }

    public function updateFile($media)
    {
        return MediaUploader::update($media);
    }

    /**
     * @param $model
     * @param $file
     * @param string $group
     * @return Media
     * @throws \Exception
     */
    public function uploadFileByModel($model, $file, string $group)
    {
        $media = $this->uploadFile($file);
        $model->attachMedia($media, $group);
        return $media;
    }


    /**
     * @param $file
     * @param $width
     * @param null $height
     * @return false|Media
     * @throws \Exception
     */
    public function uploadFileByFilter($file, $width, $height = null)
    {

        $media = $this->uploadFile($file);

        $createdDir = storage_path('/app/public') . '/' . $media->id . '/thumbnail';

        if ($this->createDir($createdDir)) {

            $path = storage_path('/app/public') . '/' . $media->id . '/' . $media->filename;

            $filteredPath = $createdDir . '/' . $media->filename;

            $this->filter($path, $filteredPath, $width, $height);

            return $media;
        } else {
            return false;
        }

    }



    public function getMediaUrl($media)
    {
        return Storage::url($media->id . '/' . $media->filename . '.' . $media->extension);
    }

    public function getRealMediaPath($media)
    {
        return storage_path('/app/public') . '/' . $media->id;
    }

    public function detachAll($model)
    {
        return $model->detachMedia(); // Detach all the media
    }

    public function detach($model, $media)
    {
        return $model->detachMedia($media); // Detach the specified media
    }

    public function clearMediaGroup($model, $group)
    {
        return $model->clearMediaGroup($group); // Detach all the media in a group
    }

    public function getAllMedia($model)
    {
        return $model->getMedia(); // All media in the default group
    }

    public function getMediaByGroup($model, $group)
    {
        return $model->getMedia($group); // All media in a custom group
    }

    public function setMedia($model, $media, $group)
    {
        return $model->attachMedia($media, $group);
    }

    public function replaceMedia($model, $media, $group = null)
    {

        return $model->syncMedia($media, $group);
    }


    public function getExtension($file)
    {
        return $file->getClientOriginalExtension();
    }

    public function getOriginalName($file)
    {
        return $file->getClientOriginalName();
    }
}
