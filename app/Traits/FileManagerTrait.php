<?php


namespace App\Traits;


use Illuminate\Support\Facades\Storage;

trait FileManagerTrait
{
    use BaseTrait;

    public function addFile($requestFile, string $userId, string $modelName)
    {
        return $requestFile->storeAs($modelName, $userId);  // path
    }

    public function getFileUrl($fileName)
    {
        return Storage::url($fileName);
    }

    /**
     * @param string $dir
     * @return bool
     */
    public function createDir(string $dir)
    {
        try {
            // already exists
            if (is_dir($dir)) {
                return false;
            }

            // is a file
            if (file_exists($dir)) {
                return false;
            }

            // created successfully?
            if (mkdir($dir, 0777, true)) {
                return true;
            }
        } catch (\Exception $e) {
            $this->logError($e, trans('messages.not_create_dir'), __LINE__, __METHOD__);
            return false;
        }

    }


    /**
     * @param string $fileName
     * @return bool
     */
    public function deleteFile(string $fileName)
    {
        if (!file_exists($fileName) || !is_file($fileName)) {
            return false;
        }

        return @unlink($fileName);
    }


    /**
     * @param string $dir
     * @return bool
     */

    public function deleteDirectory(string $dir)
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return @rmdir($dir);
    }

    /**
     * Moves a file, making sure that it exists.
     *
     * @param string $fileName Absolute path to the file
     * @param string $targetName Absolute path to the target file
     *
     * @return boolean True if file is moved, otherwise false
     */
    public function moveFiles(string $fileName, string $targetName)
    {
        if (!file_exists($fileName) || !is_file($fileName)) {
            return false;
        }

        $success = false;

        if (copy($fileName, $targetName)) {
            $success = $this->deleteFile($fileName);
        }

        return $success;
    }

    /**
     * Copies a file, making sure that it exists.
     *
     * @param string $fileName Absolute path to the file
     * @param string $targetName Absolute path to the target file
     *
     * @return boolean True if file is copied, otherwise false
     */
    public function copyFile(string $fileName, string $targetName)
    {
        if (!file_exists($fileName) || !is_file($fileName)) {
            return false;
        }

        return copy($fileName, $targetName);
    }


}
