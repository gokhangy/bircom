<?php


namespace App\Traits;


use Illuminate\Support\Facades\Log;

trait BaseTrait
{

    public function logError(\Exception $e, string $message = null, string $line, string $method, $request = null): void
    {
        Log::error($message, [
            'error' => $e->getMessage(),
            'line' => $line,
            'Method' => $method
        ]);
    }

    public function logInfo(string $message = null, array $data = []): void
    {
        Log::info($message, $data);
    }


}
