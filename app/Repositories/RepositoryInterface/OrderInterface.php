<?php


namespace App\Repositories\RepositoryInterface;


use App\Models\Order;

interface OrderInterface extends BaseInterface
{
    /**
     * @param array $attribute
     * @return mixed
     */
    public function add(array $attribute);

    /**
     * @return mixed
     */
    public function getOrders();

    /**
     * @param $orderId
     * @return mixed
     */
    public function getOrder(int $orderId);

    /**
     * @return mixed
     */
    public function getPaymentTypes();

    /**
     * @param Order $order
     * @return mixed
     */
    public function delete(Order $order);

    /**
     * @param Order $order
     * @param string $status
     * @return mixed
     */
    public function addOrderStatus(Order $order, string $status);

    /**
     * @return mixed
     */
    public function getStatus();
}
