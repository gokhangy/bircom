<?php


namespace App\Repositories\RepositoryInterface;


use App\Models\Product;
use Illuminate\Support\Facades\Request;
use Plank\Mediable\Media;

interface ProductInterface extends BaseInterface
{

    /**
     * @param array $product
     * @param Media $media
     * @return mixed
     */
    public function add(array $product, Media $media);

    /**
     * @param array $request
     * @param Product $product
     * @param $media
     * @return mixed
     */
    public function updateProduct(array $request, Product $product, $media);

    /**
     * @param Product $product
     * @return mixed
     */
    public function delete(Product $product);


    /**
     * @param int $id
     * @return mixed
     */
    public function getProduct(int $id);

    /**
     * @param array $request
     * @param $limit
     * @return mixed
     */
    public function getProducts(array $request, $limit);
}
