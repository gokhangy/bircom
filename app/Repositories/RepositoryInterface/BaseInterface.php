<?php


namespace App\Repositories\RepositoryInterface;


use Illuminate\Database\Eloquent\Model;

interface BaseInterface
{
    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * @param $id
     * @return Model
     */
    public function find(int $id): ?Model;

    /**
     * Get's all posts.
     *
     * @return mixed
     */
    public function all();

    /**
     * Get's all posts.
     *
     * @param int $limit
     * @param string $column
     * @param string $orderBy
     * @return mixed
     */
    public function paginate(int $limit, string $column = 'id', string $orderBy = 'desc');

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @param array $attributes
     * @return Model|null
     */
    public function findOneBy(array $attributes);

    /**
     * @param array $attributes
     * @return Model|null
     */
    public function findAllBy(array $attributes);

}
