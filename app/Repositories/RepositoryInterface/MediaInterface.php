<?php


namespace App\Repositories\RepositoryInterface;



interface MediaInterface extends BaseInterface
{
    /**
     * @param $file
     * @return mixed
     */
    public function upload($file);


}
