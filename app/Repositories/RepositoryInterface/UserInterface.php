<?php


namespace App\Repositories\RepositoryInterface;


use Illuminate\Database\Eloquent\Model;

interface UserInterface extends BaseInterface
{
    /**
     * @param array $attribute
     * @return mixed
     */
    public function add(array $attribute);
}
