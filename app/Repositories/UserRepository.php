<?php


namespace App\Repositories;


use App\Models\User;
use App\Repositories\RepositoryInterface\UserInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository implements UserInterface
{

    protected $model;

    public function __construct(User $model)
    {
        parent::__construct($model);

        $this->model = $model;
    }

    public function add(array $request)
    {
        return $this->model::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => Hash::make($request['password']),
            'role' => config('constant.ROLE_USER')
        ]);
    }

}
