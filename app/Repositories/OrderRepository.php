<?php


namespace App\Repositories;


use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PaymentType;
use App\Models\Status;
use App\Repositories\RepositoryInterface\OrderInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderRepository extends BaseRepository implements OrderInterface
{

    protected $model;

    public function __construct(Order $model)
    {
        parent::__construct($model);

        $this->model = $model;
    }

    public function add(array $request)
    {
        try {

            DB::transaction(function () use ($request) {

                collect($request['products'])->each(function ($item) use ($request) {

                    $ordered = $this->model::create([
                        'product_id' => $item['id'],
                        'amount' => $item['total'],
                        'address' => $request['address'],
                        'payment' => $request['payment'],
                    ]);

                    $this->addOrderStatus($ordered, config('constant.ORDER_STATUS.PENDING'));

                });
            });


        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.product_error', ['attribute' => 'Order']), __LINE__, __METHOD__);
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @return Builder[]|Collection|mixed
     * @throws \Exception
     */
    public function getOrders()
    {
        try {
            return $this->model::with(['product','status','user'])->get();
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.Order_error', ['attribute' => 'Order']), __LINE__, __METHOD__);
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param int $orderId
     * @return Builder|Builder[]|Collection|Model|mixed|null
     * @throws \Exception
     */
    public function getOrder(int $orderId)
    {
        try {

            return $this->model::with(['product','status','user'])->find($orderId);

        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.order_error', ['attribute' => 'Order']), __LINE__, __METHOD__);
            throw new \Exception($exception->getMessage());
        }
    }



    /**
     * @return PaymentType[]|false|Collection|mixed
     */
    public function getPaymentTypes()
    {
        try {
            return PaymentType::all();
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.payment_error', ['attribute' => 'Payment Type']), __LINE__, __METHOD__);
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param Order $order
     * @return bool|null
     */
    public function delete(Order $order)
    {
        try {
            return $order->delete();
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.delete_error', ['attribute' => 'Order']), __LINE__, __METHOD__);
            throw new \Exception($exception->getMessage());
        }

    }

    /**
     * @param Order $order
     * @param string $status
     * @return Model|mixed
     * @throws \Exception
     */
    public function addOrderStatus(Order $order, $status)
    {

        try {
            return $order->status()->create([
                'status' => $status
            ]);
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.status_error', ['attribute' => 'Order status']), __LINE__, __METHOD__,$status);
            throw new \Exception($exception->getMessage());
        }

    }

    /**
     * @return Status[]|Collection
     * @throws \Exception
     */
    public function getStatus()
    {
        try {
            return Status::all();
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.status_error', ['attribute' => 'Status']), __LINE__, __METHOD__);
            throw new \Exception($exception->getMessage());
        }
    }

}
