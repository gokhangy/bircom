<?php


namespace App\Repositories;

use App\Models\Product;
use App\Repositories\RepositoryInterface\MediaInterface;
use App\Repositories\RepositoryInterface\ProductInterface;
use App\Scopes\ProductScope;
use Plank\Mediable\Media;

class ProductRepository extends BaseRepository implements ProductInterface
{

    protected $model;
    private $mediaRepository;

    public function __construct(Product $model, MediaInterface $mediaRepository)
    {
        parent::__construct($model);

        $this->model = $model;
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * @param array $product
     * @param Media $media
     * @return mixed
     */
    public function add(array $product, Media $media)
    {

        try {
            return $this->model::create([
                'title' => $product['title'],
                'slug' => $product['title'],
                'content' => $product['content'],
                'price' => $product['price'],
                'image_id' => $media->id,
                'user_id' => self::getUserId()
            ]);
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.create_error', ['attribute' => 'Product']), __LINE__, __METHOD__, $product);
            return false;
        }


    }

    public function getProduct(int $id)
    {
        return $this->model::with('media')->findOrFail($id);
    }

    /**
     * @param array $request
     * @param $limit
     * @return mixed
     */
    public function getProducts(array $request, $limit)
    {
        if ($limit === null) {
            return $this->model::withoutGlobalScopes([ProductScope::class])->with('media')->get();
        }
        return $this->model::withoutGlobalScopes([ProductScope::class])->with('media')->paginate($limit);
    }

    /**
     * @param array $request
     * @param Product $product
     * @param $media
     * @return bool|mixed
     */
    public function updateProduct(array $request, Product $product, $media)
    {

        if ($media === null) {
            $imageId = $request['image_id'];
        } else {
            $imageId = $media->id;
        }

        try {

            return tap($product)->update([
                'title' => $request['title'],
                'slug' => $request['title'],
                'content' => $request['content'],
                'price' => $request['price'],
                'image_id' => $imageId,
                'user_id' => $this->getUserId()
            ]);

        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.update_error', ['attribute' => 'Product']), __LINE__, __METHOD__);
            return false;
        }


    }

    public function delete(Product $product)
    {
        try {
            return $product->delete();
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.delete_error', ['attribute' => 'Product']), __LINE__, __METHOD__);
            return false;
        }

    }

}
