<?php


namespace App\Repositories;


use App\Repositories\RepositoryInterface\MediaInterface;
use App\Traits\MediaTrait;
use Plank\Mediable\Exceptions\MediaUploadException;
use Plank\Mediable\Media;

class MediaRepository extends BaseRepository implements MediaInterface
{
    use MediaTrait;

    protected $model;

    public function __construct(Media $model)
    {
        parent::__construct($model);

        $this->model = $model;
    }

    /**
     * @param $file
     * @return mixed
     * @throws \Exception
     */
    public function upload($file)
    {
        try {
            return self::uploadFile($file);
        } catch (MediaUploadException $e) {
            $this->logError($e, trans('messages.file_error'), __LINE__, __METHOD__);
            throw $this->transformMediaUploadException($e);
        }
    }

    public function destroyFile(array $attributes)
    {
        try {
            $media = Media::where($attributes)->firstOrFail();
            $mediaPath = $this->getRealMediaPath($media);
            $media->delete();
        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.file_error'), __LINE__, __METHOD__);
            return false;
        }

        try {
            if ($mediaPath !== null) {
                $this->deleteDirectory($mediaPath);
            }
            return true;

        } catch (\Exception $exception) {
            $this->logError($exception, trans('messages.file_error'), __LINE__, __METHOD__);
            return false;
        }
    }


}
