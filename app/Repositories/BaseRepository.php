<?php


namespace App\Repositories;


use App\Repositories\RepositoryInterface\BaseInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class BaseRepository implements BaseInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        return $this->model::all();
    }

    /**
     * @param int $limit
     * @param string $column
     * @param string $orderBy
     * @return mixed
     */
    public function paginate(int $limit = 10, string $column = 'id', string $orderBy = 'desc')
    {
        return $this->model::orderBy($column, $orderBy)->paginate($limit);
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model
    {
        return $this->model::create($attributes);
    }


    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->model::findOrFail($id);
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function findOneBy(array $attributes): ?Model
    {
        return $this->model::where($attributes)->first();
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function findAllBy(array $attributes): ?Model
    {
        return $this->model::where($attributes)->get();
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return auth()->guard('api')->user()->id;
    }

    /**
     * @param \Exception $e
     * @param string|null $message
     * @param string $line
     * @param string $method
     * @param null $request
     */
    public function logError(\Exception $e, string $message = null, string $line, string $method, $request = null): void
    {
        Log::error($message, [
            'error' => $e->getMessage(),
            'line' => $line,
            'Method' => $method,
            'request' => $request
        ]);
    }
}
