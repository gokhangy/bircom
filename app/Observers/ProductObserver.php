<?php

namespace App\Observers;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProductObserver
{
    /**
     * @param Product $model
     */
    public function saving(Product $model)
    {
        $model->title = Str::title($model->title);
        $model->slug = Str::slug($model->title);
        $model->user_id = auth()->guard('api')->user()->id;
    }

    /**
     * @param Product $model
     */
    public function updating(Product $model)
    {
        $model->title = Str::title($model->title);
        $model->slug = Str::slug($model->title);
        $model->user_id = auth()->guard('api')->user()->id;
    }
}
