<?php

namespace App\Observers;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class OrderObserver
{
    /**
     * @param Order $model
     */
    public function saving(Order $model)
    {
        $model->user_id = auth()->guard('api')->user()->id;
    }

    /**
     * @param Order $model
     */
    public function updating(Order $model)
    {
        $model->user_id = auth()->guard('api')->user()->id;
    }
}
