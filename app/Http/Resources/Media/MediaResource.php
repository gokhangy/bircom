<?php

namespace App\Http\Resources\Media;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * @var mixed
     */
    private $description;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'filename' => $this->filename.'.'.$this->extension,
            'size' => $this->size,
            'directory'=> '/storage/'.$this->id.'/'.$this->filename.'.'.$this->extension
        ];

    }
}
