<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Media\MediaResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * @var mixed
     */
    private $description;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'content' => $this->content,
            'price' => $this->price,
            'image_id' => $this->image_id,
            'media'=> new MediaResource($this->media)
        ];

    }
}
