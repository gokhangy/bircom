<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Illuminate\Support\Facades\Auth;

class CheckAdmin extends Middleware
{


    public function handle($request, Closure $next)
    {

        try {

            if (Auth::check()) {

                $user = Auth::user();

                if ($user && $user->role === config('constant.ROLE_ADMIN')) {

                    return $next($request);

                } else {

                    return response()->json(['status' => 'Not Authorized'], 401);
                }

            }

        } catch (Exception $e) {
            return response()->json(['status' => 'Authorization Token not found or Token expired'], 401);
        }


    }
}
