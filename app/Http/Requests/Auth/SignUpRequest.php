<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseFormRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;


class SignUpRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'min:6|required_with:password_confirmation|same:confirmPassword',
            'confirmPassword' => 'min:6'
        ];
    }

/*    public function messages()
    {
        return [
            'name.required' => Lang::get('validation.required'),
            'email.unique' => Lang::get('validation.unique'),
            'password.min' => Lang::get('validation.min')
        ];
    }*/


}
