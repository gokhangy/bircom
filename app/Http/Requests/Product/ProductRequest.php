<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\BaseFormRequest;

class ProductRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:200',
            'content' => 'required|string|max:500',
            'image_id' => 'required|integer',
            'file' => 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }


}
