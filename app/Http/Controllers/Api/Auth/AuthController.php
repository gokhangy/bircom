<?php

namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Auth\SignInRequest;
use App\Http\Requests\Auth\SignUpRequest;
use App\Http\Resources\User\UserResource;
use App\Repositories\RepositoryInterface\UserInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{

    /**
     * @var
     */
    private $userRepository;

    /**
     * AuthController constructor.
     * @param UserInterface $userRepository
     */
    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param SignInRequest $request
     * @return JsonResponse
     */
    public function login(SignInRequest $request): JsonResponse
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {

            $user = Auth::user();

            $responseData = [
                'access_token' => $this->getUserToken($user, $user->email),
                'user' => new UserResource($user)
            ];

            $this->logInfo($user->email, ['line' => __LINE__, 'method' => __METHOD__, 'login' => 'OK']);

            return $this->apiResponse(self::SUCCESS, $responseData, trans('messages.login_success'), self::HTTP_OK);

        } else {

            $this->logInfo($request->email, ['line' => __LINE__, 'method' => __METHOD__, 'confirmation' => 'NOK']);

            return $this->apiResponse(self::ERROR, null, trans('messages.login_unsuccessful'), self::HTTP_NOT_FOUND);

        }

    }

    /**
     * @param SignUpRequest $request
     * @return JsonResponse
     */
    public function register(SignUpRequest $request)
    {

        $user = $this->userRepository->findOneBy(['email' => $request->email]);

        if (collect($user)->count() > 0) {
            $messages = trans('validation.unique', ['attribute' => $request->email]);
            $this->logInfo($messages, ['line' => __LINE__, 'method' => __METHOD__, 'register' => 'NOK']);
            return $this->apiResponse(self::ERROR, null, $messages, self::HTTP_UNAUTHORIZED);
        }


        try {

            $user = $this->userRepository->add($request->except('confirmPassword'));

        } catch (\Exception $e) {

            $message = trans('messages.internal_server_error');

            $this->logError($e, $message, __LINE__, __METHOD__);

            return $this->apiResponse(self::ERROR, null, $message, self::HTTP_INTERNAL_SERVER_ERROR);

        }


        $messages = trans('messages.register_success');
        $this->logInfo($messages, ['line' => __LINE__, 'method' => __METHOD__, 'register' => 'OK']);
        return $this->apiResponse(self::SUCCESS, new UserResource($user), $messages, self::HTTP_CREATED);

    }

    /**
     * @param $user
     * @param string|null $token_name
     * @return mixed
     */
    public function getUserToken($user, string $token_name = null)
    {
        return $user->createToken($token_name, [$user->role])->accessToken;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        $this->logInfo($request->user()->email, ['line' => __LINE__, 'method' => __METHOD__, 'logout' => 'OK']);

        return $this->apiResponse(self::SUCCESS, null, trans('messages.logout_success'), self::HTTP_OK);
    }
}
