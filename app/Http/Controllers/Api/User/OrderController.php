<?php

namespace App\Http\Controllers\Api\User;


use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Order\OrderRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use App\Repositories\RepositoryInterface\OrderInterface;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends BaseController
{

    /**
     * @var orderInterface
     */
    private $orderRepository;

    /**
     * SettingController constructor.
     * @param OrderInterface $orderRepository
     */
    public function __construct(OrderInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

    }

    /**
     * @param OrderRequest $request
     * @return JsonResponse
     */
    public function store(OrderRequest $request)
    {

        try {
            $this->orderRepository->add($request->all());
        } catch (\Exception $e) {
            $messages = trans('messages.internal_server_error', ['attribute' => 'Order']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        $this->logInfo(trans('messages.success'), ['set_order' => $request]);
        return $this->apiResponse(self::SUCCESS, null, trans('messages.success'));
    }


    /**
     * @return JsonResponse
     */
    public function payments()
    {
        try {
            $payments = $this->orderRepository->getPaymentTypes();
        } catch (\Exception $e) {
            $messages = trans('messages.not_found', ['attribute' => 'Order']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $id);
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        return $this->apiResponse(self::SUCCESS, $payments, trans('messages.success'));
    }

}
