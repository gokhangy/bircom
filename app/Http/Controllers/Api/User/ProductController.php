<?php

namespace App\Http\Controllers\Api\User;


use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use App\Repositories\RepositoryInterface\ProductInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends BaseController
{

    /**
     * @var ProductInterface
     */
    private $productRepository;

    /**
     * SettingController constructor.
     * @param ProductInterface $productRepository
     */
    public function __construct(ProductInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        try {
            $data = [];
            if ($request->has('page')) {
                if (!$request->has('limit')) {
                    $request->request->add(['limit' => self::LIMIT]);
                }
                $data = new ProductCollection($this->productRepository->getProducts($request->all(),$request->limit));
            } else {
                $data = ProductResource::collection($this->productRepository->getProducts($request->all(),null));
            }
        } catch (\Exception $e) {
            $messages = trans('messages.get_error', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        return $this->apiResponse(self::SUCCESS, $data, trans('messages.success'));

    }


    /**
     * @param ProductRequest $request
     */
    public function store(ProductRequest $request)
    {

    }

    /**
     * @param int $id
     */
    public function show(int $id)
    {


    }


    /**
     * @param ProductRequest $request
     * @param Product $product
     */
    public function update(ProductRequest $request, Product $product)
    {

    }

    /**
     * @param Product $product
     */
    public function destroy(Product $product)
    {

    }

}
