<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{

    public const HTTP_OK = Response::HTTP_OK; // 200
    public const HTTP_CREATED = Response::HTTP_CREATED; // 201
    public const HTTP_UNAUTHORIZED = Response::HTTP_UNAUTHORIZED; //403
    public const HTTP_UNPROCESSABLE_ENTITY = Response::HTTP_UNPROCESSABLE_ENTITY; // 422
    public const HTTP_NOT_FOUND = Response::HTTP_NOT_FOUND; //404
    public const HTTP_INTERNAL_SERVER_ERROR = Response::HTTP_INTERNAL_SERVER_ERROR; //500
    public const HTTP_BAD_REQUEST = Response::HTTP_BAD_REQUEST; //400

    public const SUCCESS = 'success';
    public const ERROR = 'error';
    public const WARNING = 'warning';
    public const LIMIT = 10;

    /**
     * @param $data
     * @return JsonResponse
     */
    public function ss($data)
    {
        return \response()->json($data);
    }

    /**
     * @param $resultType
     * @param $data
     * @param null $message
     * @param int $code
     * @return JsonResponse
     */
    public function apiResponse($resultType, $data, $message = null, $code = 200)
    {
        $response = [];
        $response['success'] = $resultType === self::SUCCESS;
        if (isset($data)) {
            $resultType !== self::ERROR ? $response['data'] = $data : null;
            $resultType === self::ERROR ? $response['errors'] = $data : null;
        }


        isset($message) ? $response['message'] = $message : null;
        return response()->json($response, $code);
    }

    /**
     * @param \Exception $e
     * @param string|null $message
     * @param string $line
     * @param string $method
     * @param null $request
     */
    public function logError(\Exception $e, string $message = null, string $line, string $method, $request = null): void
    {
        Log::error($message, [
            'error' => $e->getMessage(),
            'line' => $line,
            'Method' => $method,
            'request' => $request
        ]);
    }

    /**
     * @param string|null $message
     * @param array $data
     */
    public function logInfo(string $message = null, array $data = []): void
    {
        Log::info($message, $data);
    }

    /**
     *
     */
    public function sqlFirst()
    {
        DB::connection()->enableQueryLog();
    }

    /**
     *
     */
    public function sqlSecond()
    {
        $queries = DB::getQueryLog();
        Log::info($queries);
    }

}
