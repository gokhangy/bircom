<?php

namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Order\OrderStatusRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use App\Repositories\RepositoryInterface\OrderInterface;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends BaseController
{

    /**
     * @var orderInterface
     */
    private $orderRepository;

    /**
     * SettingController constructor.
     * @param OrderInterface $orderRepository
     */
    public function __construct(OrderInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        try {
            $orders = $this->orderRepository->getOrders();
            if (collect($orders)->count() > 0) {
                $orders = OrderResource::collection($orders);
            }
        } catch (\Exception $e) {
            $messages = trans('messages.not_found', ['attribute' => 'Order']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $id);
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        $this->logInfo(trans('messages.success'), ['get_product' => $orders]);
        return $this->apiResponse(self::SUCCESS, $orders, trans('messages.success'));

    }


    /**
     * @param OrderStatusRequest $request
     * @return JsonResponse
     */
    public function store(OrderStatusRequest $request)
    {

        try {

            $order = $this->orderRepository->find($request->id);

            $this->orderRepository->addOrderStatus($order, $request->status);

        } catch (\Exception $e) {
            $messages = trans('messages.internal_server_error', ['attribute' => 'Order']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        $this->logInfo(trans('messages.success'), ['set_order' => $request]);
        return $this->apiResponse(self::SUCCESS, null, trans('messages.success'));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {
            $order = $this->orderRepository->getOrder($id);
        } catch (\Exception $e) {
            $messages = trans('messages.not_found', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $id);
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        $this->logInfo(trans('messages.success'), ['get_order' => $order]);
        return $this->apiResponse(self::SUCCESS, new OrderResource($order), trans('messages.success'));
    }


    /**
     * @param Order $order
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Order $order)
    {

        $this->authorize('delete', $order);

        try {
            $this->orderRepository->delete($order);
        } catch (\Exception $e) {
            $messages = trans('messages.delete_error', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $order);
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        $this->logInfo(trans('messages.success'), ['delete_Order' => $order]);
        return $this->apiResponse(self::SUCCESS, null, trans('messages.success'));
    }

    public function getStatus()
    {
        try {
            $status = $this->orderRepository->getStatus();
        } catch (\Exception $e) {
            $messages = trans('messages.get_error', ['attribute' => 'Status']);
            $this->logError($e, $messages, __LINE__, __METHOD__, null);
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        $this->logInfo(trans('messages.success'), ['status' => $status]);
        return $this->apiResponse(self::SUCCESS, $status, trans('messages.success'));
    }
}
