<?php

namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use App\Repositories\RepositoryInterface\MediaInterface;
use App\Repositories\RepositoryInterface\ProductInterface;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends BaseController
{

    /**
     * @var ProductInterface
     */
    private $productRepository;
    /**
     * @var MediaInterface
     */
    private $mediaRepository;

    /**
     * SettingController constructor.
     * @param ProductInterface $productRepository
     * @param MediaInterface $mediaRepository
     */
    public function __construct(ProductInterface $productRepository, MediaInterface $mediaRepository)
    {
        $this->productRepository = $productRepository;
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        try {
            $data = [];
            if ($request->has('page')) {
                if (!$request->has('limit')) {
                    $request->request->add(['limit' => self::LIMIT]);
                }
                $data = new ProductCollection($this->productRepository->paginate($request->limit));
            } else {
               $data = ProductResource::collection($this->productRepository->all());
            }
        } catch (\Exception $e) {
            $messages = trans('messages.get_error', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        return $this->apiResponse(self::SUCCESS, $data, trans('messages.success'));

    }


    /**
     * @param ProductRequest $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request)
    {

        try {
            if ($request->hasFile('file')) {
                $media = $this->mediaRepository->upload($request->file('file'));
            } else {
                $messages = trans('messages.not_found', ['attribute' => 'Product Image']);
                $this->logError(new \Exception(), $messages, __LINE__, __METHOD__, $request->all());
                return $this->apiResponse(self::ERROR, null, $messages);
            }
        } catch (\Exception $e) {
            $messages = trans('messages.upload_error', ['attribute' => 'Product Image']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        try {
           $product = $this->productRepository->add($request->except('file'), $media);
        } catch (\Exception $e) {
            $messages = trans('messages.create_error', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        $this->logInfo(trans('messages.success'), ['create' => $product]);
        return $this->apiResponse(self::SUCCESS, new ProductResource($product), trans('messages.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {

        try {
            $product = $this->productRepository->getProduct($id);
        } catch (\Exception $e) {
            $messages = trans('messages.not_found', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $id);
            return $this->apiResponse(self::ERROR, null, $messages);
        }
        $this->logInfo(trans('messages.success'), ['get_product' => $product]);
        return $this->apiResponse(self::SUCCESS, new ProductResource($product), trans('messages.success'));
    }

    /**
     * @param ProductRequest $request
     * @param Product $product
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(ProductRequest $request, Product $product)
    {
        $this->authorize('update', $product);

        try {
            $media = null;

            if ($request->hasFile('file')) {
                $media = $this->mediaRepository->upload($request->file('file'));
            }
        } catch (\Exception $e) {
            $messages = trans('messages.upload_error', ['attribute' => 'Product Image']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $request->all());
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        try {
            $updatedProduct = $this->productRepository->updateProduct($request->all(), $product, $media);
        } catch (\Exception $e) {
            $messages = trans('messages.update_error', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $product);
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        $this->logInfo(trans('messages.success'), ['update_product' => $product]);
        return $this->apiResponse(self::SUCCESS, new ProductResource($updatedProduct), trans('messages.success'));
    }

    /**
     * @param Product $product
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        try {
            $this->productRepository->delete($product);
        } catch (\Exception $e) {
            $messages = trans('messages.delete_error', ['attribute' => 'Product']);
            $this->logError($e, $messages, __LINE__, __METHOD__, $product);
            return $this->apiResponse(self::ERROR, null, $messages);
        }

        $this->logInfo(trans('messages.success'), ['delete_product' => $product]);
        return $this->apiResponse(self::SUCCESS, null, trans('messages.success'));
    }

}
