<?php

namespace App\Providers;

use App\Repositories\BaseRepository;
use App\Repositories\OrderRepository;
use App\Repositories\MediaRepository;
use App\Repositories\ProductRepository;
use App\Repositories\RepositoryInterface\BaseInterface;
use App\Repositories\RepositoryInterface\OrderInterface;
use App\Repositories\RepositoryInterface\MediaInterface;
use App\Repositories\RepositoryInterface\ProductInterface;
use App\Repositories\RepositoryInterface\UserInterface;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BaseInterface::class, BaseRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(ProductInterface::class, ProductRepository::class);
        $this->app->bind(MediaInterface::class, MediaRepository::class);
        $this->app->bind(OrderInterface::class, OrderRepository::class);
    }
}
