<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\Product;
use App\Policies\OrderPolicy;
use App\Policies\ProductPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',
        Product::class => ProductPolicy::class,
        Order::class => OrderPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user) {
            if ($user->role === config('constant.ROLE_ADMIN')) {
                return true;
            }
        });

        Passport::routes();
        Passport::tokensExpireIn(now()->addDays(env('TOKEN_TTL_DAYS')));
        Passport::refreshTokensExpireIn(now()->addDays(env('TOKEN_TTL_DAYS')));
        Passport::personalAccessTokensExpireIn(now()->addDays(env('TOKEN_TTL_DAYS')));
        Passport::tokensCan([
            config('constant.ROLE_ADMIN') => 'Admin',
            config('constant.ROLE_USER') => 'User',
        ]);
    }
}
