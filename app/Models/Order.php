<?php

namespace App\Models;


use App\Observers\OrderObserver;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends BaseModel
{

    use SoftDeletes;

    protected $table = "orders";

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::observe(new OrderObserver());
    }
    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    /**
     * @return HasMany
     */
    public function status()
    {
        return $this->hasMany(OrderStatus::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }



}
