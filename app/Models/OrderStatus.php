<?php

namespace App\Models;


use App\Observers\OrderObserver;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderStatus extends BaseModel
{

    use SoftDeletes;

    protected $table = "order_status";

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function order()
    {
      return $this->belongsTo(Order::class);
    }


}
