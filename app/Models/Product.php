<?php

namespace App\Models;

use App\Observers\ProductObserver;
use App\Scopes\ProductScope;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Media;

class Product extends BaseModel
{

    use SoftDeletes;

    protected $table = "products";

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::observe(new ProductObserver());
        static::addGlobalScope(new ProductScope());
    }

    public function media()
    {
        return $this->hasOne(Media::class,'id','image_id');
    }
}
