import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {AuthService} from '@app/services/auth.service';
import {StorageService} from '@app/services/storage.service';


import * as fromRoot from '@app/core/store/reducers';
import * as authActions from '@app/core/store/actions/auth.actions';
import {Observable} from 'rxjs';
import {BaseComponent} from '../../base/base.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {

  form = {
    email: null,
    password: null,
    otp: null,
    type: 'email'
  };

  getState: Observable<any>;
  error = null;
  confirm: any = {};
  currentURL: any;
  public messages = [];
  public status: string;
  public formPost = true;

  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private route: ActivatedRoute,
    private store: Store<fromRoot.State>
  ) {
    super();
    this.getState = this.store.select(fromRoot.selectAuthListState$);

  }

  ngOnInit() {

    this.store.dispatch(new authActions.Logout(''));

    this.getState.subscribe((state) => {

      if (state.errorMessage) {
        // this.handleError();
      }
      this.error = state.errorMessage;
    });
  }

  onSubmit() {
    this.error = null;
    this.storageService.removeOtpToken();
    this.store.dispatch(new authActions.Login(this.form));

    this.store.subscribe(data => {
      if (data.auth.user !== null && data.auth.user.data.confirmation_token) {
        this.formPost = false;
      }
    });
  }



}
