import {Component, OnInit} from '@angular/core';
import {AuthService} from '@app/services/auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '@app/components/base/base.component';
import {ERROR, SUCCESS} from '@app/core/data/data';
import {Route} from '@app/core/data/route';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent extends BaseComponent implements OnInit {


  public error = [];
  public messages = [];
  public status: string;
  registerForm: FormGroup;
  submitted = false;

  constructor(private authService: AuthService,
  ) {
    super();
  }

  ngOnInit() {

    this.getProductForm();
  }

  get rf() {
    return this.registerForm.controls;
  }

  getProductForm() {

    this.registerForm = new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.email]),
        name: new FormControl(null, [Validators.required]),
        password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
        confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      }
    );
  }

  onSubmit() {
    this.spinnerShow();
    this.authService.signUp(this.registerForm.value).subscribe(
      (data: any) => {
        this.spinnerHide();
        if (data.success) {
          this.showMessage(
            this.translate('title.Registration'),
            this.translate(data.message),
            SUCCESS,
            Route.PUBLIC.LOGIN
          );
        } else {
          this.showMessage(
            this.translate('title.Registration'),
            this.translate(data.message),
            ERROR
          );
        }

      },
      error => {
        this.spinnerHide();
        this.handleError(error);
      }
    );
  }

}
