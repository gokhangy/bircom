import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../../base/base.component';
import {ERROR} from '@app/core/data/data';
import {environment} from '../../../../environments/environment';
import {ProductService} from '@app/services/product.service';
import {StorageService} from '@app/services/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseComponent implements OnInit {


  baseUrl = environment.baseUrl;
  products = [];
  baskets = [];

  constructor(private productService: ProductService, private storageService: StorageService) {
    super();
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.spinnerShow();
    this.productService.getProductsForUser().subscribe(data => {
      this.products = data.data;
      this.spinnerHide();
    }, error => {
      this.showMessage(
        this.translate('title.Product'),
        this.translate(error.error),
        ERROR,
      );
    });
  }

  addBasket(product: any) {
    if (typeof product.total === 'undefined') {
      this.showMessage(
        this.translate('Product'),
        this.translate('Please enter the amount first'),
        ERROR,
      );
      return false;
    }
    this.baskets.push(product);
    this.storageService.setBaskets(this.baskets);
  }

  change($event: any, product) {
    this.products.forEach((data, index) => {
      if (data.id === product.id) {
        data.total = $event.target.value;
      }
    });

  }
}
