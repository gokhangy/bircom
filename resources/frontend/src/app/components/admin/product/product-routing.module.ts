import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProductComponent} from '@app/components/admin/product/product.component';
import {AddComponent} from '@app/components/admin/product/add/add.component';
import {EditComponent} from '@app/components/admin/product/edit/edit.component';


const routes: Routes = [
  {
    path: '',
    component: ProductComponent,
  },

  {
    path: 'add',
    component: AddComponent
  },
  {
    path: 'edit/:product',
    component: EditComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
}
