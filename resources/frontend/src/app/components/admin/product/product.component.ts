import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '@app/components/base/base.component';
import {ProductService} from '@app/services/product.service';
import {ERROR, SUCCESS} from '@app/core/data/data';
import {Route} from '@app/core/data/route';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent extends BaseComponent implements OnInit {

  baseUrl = environment.baseUrl;
  products = [];

  constructor(private productService: ProductService) {
    super();
  }


  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.spinnerShow();
    this.productService.getProducts().subscribe(data => {
      this.spinnerHide();
      this.products = data.data;
    }, error => {
      this.showMessage(
        this.translate('title.Product'),
        this.translate(error.error),
        ERROR,
      );
    });
  }

  delete(item: any) {
    this.spinnerShow();
    this.productService.delete(item.id).subscribe(data => {
      this.spinnerHide();
      this.products.splice(this.products .indexOf(item), 1);
      this.showMessage(
        this.translate('title.Product'),
        this.translate('Record deleted'),
        SUCCESS,
      );
    }, error => {
      this.spinnerHide();
      this.showMessage(
        this.translate('title.Product'),
        this.translate(error.error),
        ERROR,
      );
    });
  }
}
