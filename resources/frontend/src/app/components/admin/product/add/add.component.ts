import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '@app/components/base/base.component';
import {UploadService} from '@app/services/upload.service';
import {ERROR, SUCCESS} from '@app/core/data/data';
import {Route} from '@app/core/data/route';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent extends BaseComponent implements OnInit {

  productForm: FormGroup;

  constructor(private uploadService: UploadService) {
    super();
  }

  ngOnInit() {
    this.getProductForm();
  }

  get rf() {
    return this.productForm.controls;
  }

  getProductForm() {

    this.productForm = new FormGroup({
        title: new FormControl(null, [Validators.required]),
      content: new FormControl(null, [Validators.required]),
        price: new FormControl(null, [Validators.required]),
        image_id: new FormControl(1, [Validators.required]),
        file: new FormControl('', [Validators.required]),
        fileSource: new FormControl('', [Validators.required])
      }
    );
  }

  onFileChange(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.productForm.patchValue({
        fileSource: file
      });
    }
  }

  onSubmit() {
    this.spinnerShow();
    const file = this.productForm.get('fileSource').value;
    const formData: FormData = new FormData();
    formData.append('file', file,file.name);
    formData.append('title', this.productForm.get('title').value);
    formData.append('content', this.productForm.get('content').value);
    formData.append('price', this.productForm.get('price').value);
    formData.append('image_id', this.productForm.get('image_id').value);
    this.uploadService.upload(formData).subscribe(data=>{
      this.spinnerHide();
      if (data.success) {
        this.showMessage(
          this.translate('web.Add Product'),
          this.translate(data.message),
          SUCCESS,
          Route.ADMIN.PRODUCTS
        );
      } else {
        console.log(data);
        this.showMessage(
          this.translate('web.Add Product'),
          'Validation Error',
          ERROR
        );
      }
    },error => {
      this.spinnerHide();
      this.showMessage(
        this.translate('web.Add Product'),
        this.translate(error.error),
        ERROR
      );
    })

  }


}
