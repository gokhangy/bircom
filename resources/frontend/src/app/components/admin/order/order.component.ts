import {Component, OnInit} from '@angular/core';
import {ERROR, SUCCESS} from '@app/core/data/data';
import {OrderService} from '@app/services/order.service';
import {BaseComponent} from '@app/components/base/base.component';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent extends BaseComponent implements OnInit {

  orders = [];
  baseUrl = environment.baseUrl;
  allStatus = [];

  constructor(private orderService: OrderService) {
    super();
  }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders() {
    this.spinnerShow();
    this.orderService.getOrders().subscribe(data => {
      this.spinnerHide();
      this.orders = data.data;
      console.log(this.orders);
    }, error => {
      this.spinnerHide();
      this.showMessage(
        this.translate('Order'),
        this.translate(error.error),
        ERROR,
      );
    });
  }



  delete(item: any) {
    this.spinnerShow();
    this.orderService.delete(item.id).subscribe(data => {
      this.spinnerHide();
      if(data.success){
        this.orders.splice(this.orders.indexOf(item), 1);
        this.showMessage(
          this.translate('Orders'),
          this.translate('Record deleted'),
          SUCCESS,
        );
        this.orders.splice(this.orders.indexOf(item), 1);
      }else{
        this.showMessage(
          this.translate('Orders'),
          this.translate('An error has occurred in the server. Please try again later'),
          ERROR,
        );
      }

    }, error => {
      this.spinnerHide();
      this.showMessage(
        this.translate('Orders'),
        this.translate('An error has occurred in the server. Please try again later'),
        ERROR,
      );
    });
  }

}
