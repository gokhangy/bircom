import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {OrderComponent} from '@app/components/admin/order/order.component';
import {AddComponent} from '@app/components/admin/order/add/add.component';


const routes: Routes = [
  {
    path: '',
    component: OrderComponent,
  },
  {
    path: 'edit/:id',
    component: AddComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule {
}
