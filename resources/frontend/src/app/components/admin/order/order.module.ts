import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';
import {SharedModule} from '@app/shared/shared.module';
import {FormsModule} from '@angular/forms';
import { AddComponent } from './add/add.component';


@NgModule({
  declarations: [OrderComponent, AddComponent],
  imports: [
    CommonModule,
    OrderRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class OrderModule { }
