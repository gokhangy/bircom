import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UploadService} from '@app/services/upload.service';
import {BaseComponent} from '@app/components/base/base.component';
import {ERROR, SUCCESS} from '@app/core/data/data';
import {Route} from '@app/core/data/route';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent extends BaseComponent implements OnInit {

  @Input() isMultiple = true;
  @Input() fileType = null;
  @Input() maxSize = '2097152'; // byte // 2mgb
  @Input() accept = 'image/*';
  @Input() maxUploadFile = 5;
  @Output() uploadedFile: EventEmitter<any> = new EventEmitter<any>();
  @Input() buttonType = null;

  files: File[] = [];
  file: File;
  lastInvalids: any;

  constructor(private uploadService: UploadService) {
    super();
  }

  ngOnInit(): void {
  }


  cancel() {
    //  this.lastInvalids.splice(i,1); remove
  }

  uploadFiles(files: File[]) {
    this.uploads(files);
  }


  uploadFile(file: File) {
    this.upload(file);
  }

  uploadsFile(files: File[]) {
    this.uploads(files);
    return this.uploadedFile.emit(this.file);
  }

  getDate() {
    return new Date();
  }

  // for one upload
  upload(file: File) {
    this.spinnerShow();
    this.uploadService.uploadFile(file, this.fileType).subscribe(data => {
        console.log(data);
        this.spinnerHide();
        if (data.success) {

          this.showMessage(
            this.translate('title.FileUpload'),
            this.translate(data.message),
            SUCCESS
          );
          return this.uploadedFile.emit([this.file, data.data]);
        } else {
          this.showMessage(
            this.translate('title.FileUpload'),
            this.translate(data.message),
            ERROR
          );
        }
      },
      error => {
        this.spinnerHide();
        this.handleError(error);
      });
  }

  // for multiupload
  uploads(files: File[]) {

    this.spinnerShow();
    this.uploadService.uploadsFile(files, this.fileType).subscribe(data => {
        this.spinnerHide();
        if (data.success) {
          this.showMessage(
            this.translate('title.FileUpload'),
            this.translate(data.message),
            SUCCESS
          );
          return this.uploadedFile.emit([this.files, data.data]);
        } else {
          this.showMessage(
            this.translate('title.FileUpload'),
            this.translate(data.message),
            ERROR
          );
        }
      },
      error => {
        this.spinnerHide();
        this.handleError(error);
        return false;
      });
  }

  deleteFile(fileId: any) {
    this.spinnerShow();
    this.uploadService.removeFile({id: fileId}).subscribe(data => {
        this.spinnerHide();
        if (data.success) {
          this.showMessage(
            this.translate('title.FileDelete'),
            this.translate(data.message),
            SUCCESS
          );
          return true;
        } else {
          this.showMessage(
            this.translate('title.FileDelete'),
            this.translate(data.message),
            ERROR
          );
          return false;
        }
      },
      error => {
        this.spinnerHide();
        this.handleError(error);
        return false;
      });
  }

}
