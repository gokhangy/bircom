import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NotfoundComponent} from './notfound/notfound.component';
import {AlertComponent} from './alert/alert.component';
import {ValidationErrorComponent} from './validation-error/validation-error.component';
import {ValidationComponent} from '@app/shared/validation/validation.component';
import {MaterialModule} from '@app/shared/material/material.module';

@NgModule({
  declarations: [NotfoundComponent, AlertComponent, ValidationErrorComponent, ValidationComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    AlertComponent,
    ValidationErrorComponent,
    ValidationComponent,
    MaterialModule
  ],
})
export class SharedModule {
}
