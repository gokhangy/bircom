import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse, HttpErrorResponse
} from '@angular/common/http';


import {AuthService} from '@app/services/auth.service';
import {Router} from '@angular/router';
import {StorageService} from '@app/services/storage.service';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Route} from '@app/core/data/route';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(public authService: AuthService, public router: Router, public storageService: StorageService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.storageService.getToken();

    if (token) {
      request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token)});
    }

    if (!request.headers.has('Content-Type')) {
      if (request.url === environment.apiUrl + Route.ADMIN.UPLOAD) {
        this.upload(request);
      } else if(request.url.includes(environment.apiUrl + Route.ADMIN.UPLOAD)){
        this.upload(request);
      }else {
        request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
      }
    }

    request = request.clone({headers: request.headers.set('Accept', 'application/json')});

    return next.handle(request).pipe(tap(() => {
      },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.router.navigate(['login']);
        }
      }));
  }

  private upload(request: HttpRequest<any>): HttpRequest<any> {
    request = request.clone({headers: request.headers.set('reportProgress', 'true')});
    request = request.clone({headers: request.headers.set('observe', 'event')});
    return request;
  }
}
