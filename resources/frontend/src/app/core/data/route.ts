export const Route = {

  ADMIN: {
    GET_ALL: 'admin/getAllUser',
    PRODUCTS: '/admin/products',
    UPLOAD: '/admin/products',
    ORDERS: '/admin/orders',
    STATUS: '/admin/status'
  },
  USER: {
    PRODUCTS: '/user/products',
    DASHBOARD: '/dashboard',
    PAYMENT: '/user/payments',
    CHECKOUT: '/user/orders',
    GET_USER: '/user',
    PROFILE: '/user/profile',
    UPLOAD: '/user/upload-game',
    DELETE_FILE: '/user/delete-file-game',
    DELETE: '/user/delete'
  },
  PUBLIC: {
    GET: '/getPublic',
    LOGIN: '/login',
  },
  DASHBOARD: '/dashboard',


};
