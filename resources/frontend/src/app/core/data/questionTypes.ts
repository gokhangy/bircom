export const Types = [
  {id: 'multiple-choice', name: 'Çoktan Seçmeli', new: false},
  {id: 'open-type', name: 'Açık Uçlu', new: false},
  {id: 'pairing-type', name: 'Eşleştirme', new: false},
];

export const TypeId = {
  multiple_choice: 'multiple-choice',
  open_type: 'open-type',
  pairing_type: 'pairing-type'
};
