import {NgModule, Component} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/auth/login/login.component';
import {SignupComponent} from './components/auth/signup/signup.component';
import {PublicGuard} from './core/guard/public.guard';
import {UserGuard} from './core/guard/user.guard';
import {HomeComponent} from './components/public/home/home.component';
import {AdminGuard} from './core/guard/admin.guard';
import {NotfoundComponent} from './shared/notfound/notfound.component';
import {AppLayoutComponent} from './layout';
import {AuthLayoutComponent} from './layout/auth-layout/auth-layout.component';


export const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: '', component: AppLayoutComponent,
    children: [
      {path: '', pathMatch: 'full', redirectTo: 'dashboard'},
      {path: 'dashboard', loadChildren: () => import('./components/user/dashboard/dashboard.module').then(m => m.DashboardModule)},
      {path: 'baskets', loadChildren: () => import('./components/user/basket/basket.module').then(m => m.BasketModule)},
      {path: 'checkout', loadChildren: () => import('./components/user/checkout/checkout.module').then(m => m.CheckoutModule)},
    ],
    canActivate: [UserGuard]
  },
  {
    path: 'admin', component: AppLayoutComponent,
    children: [
      {path: '', pathMatch: 'full', redirectTo: 'products'},
      {
        path: 'products',
        loadChildren: () => import('./components/admin/product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'orders',
        loadChildren: () => import('./components/admin/order/order.module').then(m => m.OrderModule)
      }
    ],
    canActivate: [AdminGuard]
  },
  {
    path: '',
    component: AuthLayoutComponent, children: [
      {
        path: 'signUp', component: SignupComponent,
        loadChildren: () => import('./components/auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: 'login', component: LoginComponent,
        loadChildren: () => import('./components/auth/auth.module').then(m => m.AuthModule)
      },
     ],
    canActivate: [PublicGuard]
  },
  {

    path: 'notFound',
    component: AuthLayoutComponent, children: [
      {path: '', component: NotfoundComponent},
    ],

  },
  {
    path: '**',
    redirectTo: 'notFound'
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

