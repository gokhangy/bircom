import {Injectable} from '@angular/core';
import {ApiService} from '@app/services/api.service';
import {Observable, of} from 'rxjs';
import {Route} from '@app/core/data/route';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private apiService: ApiService) {
  }

  getProducts(): Observable<any> {
    return this.apiService.get(`${Route.ADMIN.PRODUCTS}`).pipe(catchError(this.formatError));
  }
  getProductsForUser(): Observable<any> {
    return this.apiService.get(`${Route.USER.PRODUCTS}`).pipe(catchError(this.formatError));
  }

  getProduct(id: any): Observable<any> {
    return this.apiService.get(`${Route.ADMIN.PRODUCTS}` + '/' + id).pipe(catchError(this.formatError));
  }
  getPayment(): Observable<any> {
    return this.apiService.get(`${Route.USER.PAYMENT}`).pipe(catchError(this.formatError));
  }
  delete(id: any): Observable<any> {
    return this.apiService.delete(`${Route.ADMIN.PRODUCTS}` + '/' + id).pipe(catchError(this.formatError));
  }

  checkout(data:any): Observable<any> {
    return this.apiService.post(`${Route.USER.CHECKOUT}`, data).pipe(catchError(this.formatError));
  }

  formatError(error: any) {
    return of(environment.apiUrl + error.error);
  }
}
