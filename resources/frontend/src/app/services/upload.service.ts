import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable, of} from 'rxjs';
import {Route} from '@app/core/data/route';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private apiUrl = environment.apiUrl;

  constructor(private apiService: ApiService) {
  }

  upload(formData: FormData): Observable<any> {
    return this.apiService.postUpload(`${Route.ADMIN.UPLOAD}`, formData).pipe(catchError(this.formatError));
  }

  uploadUpdate(formData: FormData, id: any): Observable<any> {
    return this.apiService.postUpload(`${Route.ADMIN.UPLOAD}` + '/' + id, formData).pipe(catchError(this.formatError));
  }


  //--------------------------------------------------------
  uploadFile(file: File, type: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('type', type);
    return this.apiService.postUpload(`${Route.ADMIN.UPLOAD}`, formData).pipe(catchError(this.formatError));
  }

  uploadsFile(files: File[], type: string): Observable<any> {
    const uploadFormData: FormData = new FormData();
    files.forEach(data => {
      uploadFormData.append('file', data, data.name);
      uploadFormData.append('type', type);
    });

    return this.apiService.postUpload(`${Route.USER.UPLOAD}`, uploadFormData).pipe(catchError(this.formatError));
  }

  removeFile(file: any): Observable<any> {
    return this.apiService.post(`${Route.USER.DELETE_FILE}`, file).pipe(catchError(this.formatError));
  }

  removeFileById(fileId: any): Observable<any> {
    return this.apiService.delete(`${Route.USER.DELETE_FILE}`, fileId).pipe(catchError(this.formatError));
  }

  formatError(error: any) {
    return of(environment.apiUrl + error.error);
  }

}
