import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Route} from '@app/core/data/route';
import {catchError} from 'rxjs/operators';
import {ApiService} from '@app/services/api.service';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private apiService: ApiService) {
  }

  getOrders(): Observable<any> {
    return this.apiService.get(`${Route.ADMIN.ORDERS}`).pipe(catchError(this.formatError));
  }

  getOrder(id: any): Observable<any> {
    return this.apiService.get(`${Route.ADMIN.ORDERS}` + '/' + id).pipe(catchError(this.formatError));
  }

  getStatus(): Observable<any> {
    return this.apiService.get(`${Route.ADMIN.STATUS}`).pipe(catchError(this.formatError));
  }

  delete(id: any): Observable<any> {
    return this.apiService.delete(`${Route.ADMIN.ORDERS}` + '/' + id).pipe(catchError(this.formatError));
  }

  setStatus(data:any): Observable<any> {
    return this.apiService.post(`${Route.ADMIN.ORDERS}`, data).pipe(catchError(this.formatError));
  }

  formatError(error: any) {
    return of(environment.apiUrl + error.error);
  }
}
