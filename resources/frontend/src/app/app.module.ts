import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule} from '@angular/core';

import {FormsModule} from '@angular/forms';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './core/interceptors/AuthInterceptor';
import {StorageService} from './services/storage.service';
import {AuthService} from './services/auth.service';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {AuthEffects} from './core/store/effects/auth.effects';
import {UserEffects} from './core/store/effects/user.effects';
import {NavbarComponent} from './layout';
import {HomeComponent} from './components/public/home/home.component';
import {reducers} from './core/store/reducers';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HeaderComponent} from './layout';
import {FooterComponent} from './layout';
import {AppLayoutComponent} from './layout';
import {AuthLayoutComponent} from './layout/auth-layout/auth-layout.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppInjectorService} from './services/app-injector.service';
import {InputRefDirective} from './core/directive/input-ref.directive';
import { ApiService } from './services/api.service';


export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AppLayoutComponent,
    AuthLayoutComponent,
    InputRefDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forRoot([AuthEffects, UserEffects]),
    StoreModule.forRoot(reducers, {}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthService, StorageService, ApiService
  ],
  exports: [StoreModule],
  bootstrap: [AppComponent],

})
export class AppModule {
  constructor(injector: Injector) {
    AppInjectorService.injector = injector;
  }
}
