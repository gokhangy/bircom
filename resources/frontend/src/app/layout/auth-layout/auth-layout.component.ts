import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnInit {

  constructor(private translateService: TranslateService) {
    this.translateService.addLangs(['tr', 'en']);
    const browserLang = this.translateService.getBrowserLang();
    this.translateService.use(browserLang.match(/tr|en/) ? browserLang : 'en');
  }


  ngOnInit(): void {
  }

}
