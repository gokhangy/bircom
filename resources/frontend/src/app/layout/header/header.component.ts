import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {StorageService} from '@app/services/storage.service';
import {AuthService} from '@app/services/auth.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../core/store/reducers';
import {State} from '@app/core/store/reducers';
import * as authActions from '../../core/store/actions/auth.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  loggedIn: boolean;
  role: any;
  state: State;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService,
    private store: Store<fromRoot.State>
  ) {
  }

  ngOnInit() {
    this.store.select(state => state.auth).subscribe((state) => {
      this.loggedIn = this.storageService.loggedIn();
      this.role = this.storageService.getRole();
    });
  }

  logout(): void {
    this.store.dispatch(new authActions.Logout(''));
  }

}
