<?php

return [
    'login_success' => 'Successfully login',
    'logout_success' => 'Successfully logged out',
    'login_unsuccessful' => 'Email or password is wrong. Please check',
    'get_error' => 'An error occurred while fetching the :attribute',
    'success' => 'Successfully',
    'create_error' => 'There was an error while creating the :attribute',
    'not_found' => ':attribute not found',
    'validation_general_error'=>'Validation error',
    'not_found_record'=>'Record not found',
    'page_not_found'=>'Page not found',
    'unauthorized'=>'Unauthorized Access',
    'wrong_query'=>'Query error',
    'validation_error'=>'validation failed',
    'internal_server_error'=>'An error has occurred in the server. Please try again later',
    'update_error'=>'An error occurred while updating the :attribute',
    'delete_error'=>'An error occurred while removing the :attribute',
    'upload_error'=>'An error occurred while uploading the :attribute',
    'basket_error'=>'An error occurred while basket the :attribute',
    'file_error'=>'File not found',
    'register_success'=>'Successfully registration'
];
